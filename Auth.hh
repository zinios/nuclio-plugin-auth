<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\auth
{
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	
	use nuclio\plugin\user\model\
	{
		User,
		UserRole,
		UserPermission
	};
	
	use nuclio\plugin\auth\
	{
		model\Permission,
		model\PermissionRole,
		model\Role,
		AuthException
	};
	
	use nuclio\plugin\
	{
		session\Session,
		session\CommonSessionInterface,
		database\orm\Cursor
	};
	
	/**
	 * Auth Class
	 * Handle user authentication
	 * @singleton
	 */
	<<
		__ConsistentConstruct,
		singleton
	>>
	class Auth extends Plugin
	{
		const USER_SUPER='SUPER';
		const USER_GUEST='GUEST';
		const ROLE_SUPER='SUPER';
		const ROLE_GUEST='GUEST';
		
		protected CommonSessionInterface $session;
		protected bool $impersonating=false;
		protected User $user;
		protected User $originalUser;
		protected Vector<UserRole> $roles;
		protected Vector<UserPermission> $permissions;
		protected Vector<Permission> $permissionFullMatrix;
		protected Vector<string> $permissionKeyMatrix;
		
		/**
	     * call the constructor of the auth class.
	     */
		public static function getInstance(/* HH_FIXME[4033] */...$args):this
		{
			$instance=ClassManager::getClassInstance(static::class,...$args);
			return ($instance instanceof static)?$instance:new static(...$args);
		}
		
		protected function getUserFromSession():?User
		{
			return User::findOne(Map{'id'=>$this->session->get('user')});
		}
		
		protected function findUserWithCredentials(string $email, string $password, string $userSalt):?User
		{
			return User::findOne
			(
				Map
				{
					'email'		=>$email,
					'password'	=>md5((string)$userSalt.(string)$password)
				}
			);
		}
		
		protected function findUserWithRef(string $ref):?User
		{
			return User::findOne(Map{'ref'=>$ref});
		}
		
		protected function findUserWithEmail(string $email):?User
		{
			return User::findOne(Map{'email'=>$email});
		}
		
		protected function findUserWithId(int $id):?User
		{
			return User::findOne(Map{'id'=>$id});
		}
		
		protected function getRoles():Cursor
		{
			return UserRole::find(Map{'user'=>$this->user},Map{'limit'=>10000});
		}
		
		protected function getPermissions():Cursor
		{
			return UserPermission::find(Map{'user'=>$this->user},Map{'limit'=>10000});
		}
		
		protected function findRolePermissions($role):Cursor
		{
			return PermissionRole::find(Map{'role'=>$role},Map{'limit'=>10000});
		}
		
		 /**
	     * Can function checks the current authentecated user permissions to perform CURD.
	     * @return bool   if the user has permission to perform the action or not.
	     */
		public static function can(string $do):bool
		{
			$auth=Auth::getInstance();
			if ($auth->isSuperUser() || $auth->permissionKeyMatrix->linearSearch($do)!==-1)
			{
				return true;
			}
			throw new AuthException(sprintf('Permission Denied (%s).',$do));
		}
		
		 /**
	     * constructor get instance of auth.
	     */
		public function __construct(CommonSessionInterface $sessionHandler)
		{
			parent::__construct();

			$this->session=$sessionHandler;
			$this->impersonating=$this->session->get('impersonating') ?? false;
			if($this->isAuthenticated())
			{
				$user=$this->getUserFromSession();
				if (!is_null($user))
				{
					$this->user=$user;
					if (!$this->impersonating)
					{
						$this->originalUser=clone($user);
					}
					else
					{
						$originalUser=$this->findUserWithId((int)$this->session->get('originalUser'));
						if ($originalUser instanceof User)
						{
							$this->originalUser=$originalUser;
						}
						else
						{
							$this->unauthenticate();
							throw new AuthException('The user account is missing. It has probably been removed.');
						}
					}
				}
				else
				{
					$this->unauthenticate();
					throw new AuthException('The user account is missing. It has probably been removed.');
				}
			}
			else
			{
				$this->user=$this->findUserWithRef(self::USER_GUEST);
				if ($this->user instanceof User)
				{
					$this->originalUser=clone($this->user);
				}
				else
				{
					//TODO: what do we do here? Log a warning?
				}
			}
			$this->fetchRolesAndPermissions();
			$this->generateUserPermissionsMatrix();
		}
		
		/**
		 * Impersonates a user.
		 *
		 * @param integer $userId
		 * @return boolean
		 */
		public function impersonateUser(int $userId):bool
		{
			$user=$this->findUserWithId($userId);
			if ($user instanceof User)
			{
				$this->user=$user;
				$this->impersonating=true;
				$this->session->set('impersonating',true);
				$this->session->set('user',(int)$user->getId());
				$this->fetchRolesAndPermissions();
				$this->generateUserPermissionsMatrix();
				return true;
			}
			return false;
		}
		
		/**
		 * Ends impersonation.
		 *
		 * @return boolean
		 */
		public function endImpersonation():bool
		{
			$this->user=$this->originalUser;
			$this->impersonating=false;
			$this->session->set('impersonating',false);
			$this->session->set('user',(int)$this->user->getId());
			$this->fetchRolesAndPermissions();
			$this->generateUserPermissionsMatrix();
			return true;
		}
		
		/**
		 * Fetch Roles And Permissions for the current authenticated user
		 *
		 * @return Auth 	return the Auth Object.
		 */
		private function fetchRolesAndPermissions():this
		{
			$userRoles	=$this->getRoles();
			$roles		=Vector{};
			foreach ($userRoles as $role)
			{
				$roles[]=$role;
			}
			$this->roles		=$roles;
			$userPermissions	=$this->getPermissions();
			$permissions		=Vector{};
			foreach ($userPermissions as $permission)
			{
				$permissions[]=$permission;
			}
			$this->permissions=$permissions;
			return $this;
		}
		
		public function isImpersonating():bool
		{
			return $this->impersonating;
		}
		
		/**
		 * Authenticate the user
		 * @param  string $email    Email of the user to be authenticate
		 * @param  string $password Password of the user to be authenticate
		 * @return Map<string,mixed> Authenticated user.
		 */
		public function authenticate(string $email, string $password):User
		{
			$user=$this->findUserWithEmail($email); //NEED TO FIND USER
			if(!is_null($user))
			{
				$userSalt=$user->get('salt');
				$result=$this->findUserWithCredentials((string)$email,(string)$password,(string)$userSalt);
				if(!is_null($result))
				{
					$this->session->set('authenticated',true);
					$this->session->set('user',(int)$user->getId());
					$this->session->set('originalUser',(int)$user->getId());
					$this->user=$user;
					$this->originalUser=clone($user);
					$this->fetchRolesAndPermissions();
					$this->generateUserPermissionsMatrix();
					return $user;
				}
				else
				{
					throw new AuthException('Invalid Login');
				}
			}
			else
			{
				throw new AuthException('Email not registered');
			}
		}
		
		/**
		 * Check either the password given is valid or not.
		 * @param  string      $password Password to be check
		 * @param  string|null $email    Email of user
		 * @return boolean               True/false for valid password.
		 */
		public function isValidPassword(string $password, ?string $email=null):bool
		{
			if(is_null($email))
			{
				$user=$this->getUser();
			}
			else
			{
				$user=$this->findUserWithEmail($email);
			}
			if(!is_null($user))
			{
				$userSalt=$user->get('salt');
				$userPassword=$user->get('password');
			}
			else
			{
				throw new AuthException('User does not exist');
			}
			if(md5((string)$userSalt.(string)$password)==$userPassword)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * Authenticate user by given user ID.
		 * @param  string $userId ID of user to be authenticate.
		 */
		public function authenticateUser(arraykey $userId):void
		{
			$user=$this->findUserWithId($userId);
			if(!is_null($user))
			{
				$this->session->set('authenticated',true);
				$this->session->set('user',$user->getId());
				$this->session->set('originalUser',$user->getId());
			}
			else
			{
				throw new AuthException('User with this ID is not found!');
			}
		}
		
		/**
		 * Remove authentication from the current authenticated user.
		 */
		public function unauthenticate():void
		{
			$this->session->delete('authenticated');
			$this->session->delete('user');
			$this->session->delete('originalUser');
			$this->user=$this->findUserWithRef(self::USER_GUEST);
			if ($this->user instanceof User)
			{
				$this->originalUser=clone($this->user);
			}
			else
			{
				//TODO: what do we do here? Log a warning?
			}
		}
		
		/**
		 * Check either the session is authenticated or not.
		 * @return boolean               True/false for authenticated.
		 */
		public function isAuthenticated():bool
		{
			return (bool)$this->session->get('user');
		}
		
		/**
		 * Get the current authenticated user.
		 * @return User        return the current authenticated user.
		 */
		public function getUser():?User
		{
			return $this->user;
		}
		
		/**
		 * Get the current authenticated user ID.
		 * @return mixed           return the Id of the current authenticated user.
		 */
		public function getUserId():mixed
		{
			if(!is_null($this->user))
			{
				return $this->user->getId();
			}
		}
		
		/**
		 * Check the authenticated user is super user or not.
		 * @return boolean True/false depends on it is super user or not.
		 */
		public function isSuperUser():bool
		{
			$user=$this->getUser();
			$roles=$this->getUserRoles();
			if ($user->getRef()===self::USER_SUPER)
			{
				return true;
			}
			foreach ($roles as $role)
			{
				if ($role->getRef()===self::ROLE_SUPER)
				{
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Get roles of the current authenticated user.
		 * @return Vector<UserRole> User roles
		 */
		public function getUserRoles():Vector<UserRole>
		{
			return $this->roles;
		}
		
		/**
		 * Get permissions of the current authenticated user.
		 * @return Vector<UserPermission> User permissions
		 */
		public function getUserPermissions():Vector<UserPermission>
		{
			return $this->permissions;
		}
		
		/**
		 * Get permissions of the current authenticated user.
		 * @return Vector<UserPermission> User permissions
		 */
		public function generateUserPermissionsMatrix():this
		{
			$roles				=$this->getUserRoles();
			$permissionMatrix	=Vector{};
			//Capture the permission id of every assigned permission in every role assigned to the user.
			for ($i=0,$j=count($roles); $i<$j; $i++)
			{
				$rolePermissions=$this->findRolePermissions($roles[$i]->getRole());
				foreach ($rolePermissions as $rolePermission)
				{
					$permissionMatrix[]=$rolePermission->getPermission();
				}
			}
			//Now capture every permission id which is explicitly assigned to the user.
			$permissions=$this->getUserPermissions();
			foreach ($permissions as $permission)
			{
				$permissionMatrix[]=$permission->getPermission();
			}
			$this->permissionFullMatrix=$permissionMatrix;
			$this->permissionKeyMatrix=Vector{};
			for ($i=0,$j=count($this->permissionFullMatrix); $i<$j; $i++)
			{
				if ($this->permissionFullMatrix[$i] instanceof Permission)
				{
					$this->permissionKeyMatrix[]=$this->permissionFullMatrix[$i]->get('key');
				}
				else
				{
					$thiSPermission=Permission::findById($this->permissionFullMatrix[$i]);
					if ($thiSPermission instanceof Permission)
					{
						$this->permissionKeyMatrix[]=$thiSPermission->get('key');
					}
				}
			}
			return $this;
		}
	}
}
