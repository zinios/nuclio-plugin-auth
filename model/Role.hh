<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\auth\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * This calss present the structure and the datatype of the Role table in the database
	 * @Collection role
	 */
	class Role extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var The unique database ID 
		 */
		public ?string $id=null;
		
		/**
		 * @String
		 * 
		 * @var string  Role name
		 */
		public ?string $name=null;
		
		/**
		 * @String
		 * 
		 * @var string Role description
		 */
		public ?string $description=null;
		
		/**
		 * @String
		 * 
		 * @var string unique referance for each role
		 */
		public ?string $ref=null;
		
		/**
		 * @Integer
		 * 
		 * @var string status of the role
		 */
		public ?int $status=null;
	}
}
