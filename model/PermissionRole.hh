<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\auth\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * This calss present the structure and the datatype of the PermissionRole table in the database
	 * 
	 * @Collection permissionRole
	 */
	class PermissionRole extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var The unique database ID
		 */
		public mixed $id=null;
		
		/**
		 * @Relate nuclio\plugin\auth\model\Role
		 * 
		 * @var  string Referance to Role class
		 */
		public ?Model $role=null;
		
		/**
		 * @Relate nuclio\plugin\auth\model\Permission
		 * 
		 * @var  string Referance to Permission class
		 */
		public ?Model $permission=null;

	}
}
