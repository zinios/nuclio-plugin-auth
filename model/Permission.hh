<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\auth\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * This calss present the structure and the datatype of the Permission table in the database
	 * 
	 * @Collection  permission
	 */
	class Permission extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var The unique database ID 
		 */
		public ?string $id=null;
		
		/**
		 * @String
		 * 
		 * @var  string permission name
		 */
		public ?string $name=null;
		
		/**
		 * @String
		 * 
		 * @var string permission description
		 */
		public ?string $description=null;
		
		/**
		 * @String
		 * 
		 * @var string the key for the permission 
		 */
		public ?string $key=null;
		
		/**
		 * @String
		 * 
		 * @var category of permission
		 */
		public ?int $category=null;
	}
}
